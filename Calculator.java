import java.lang.Math;
import java.util.Random;

public class Calculator{
	
	public static int SumOf (int sumNum1, int sumNum2){
		return sumNum1 + sumNum2;	
	}
	
	public static double SqrtOf (int sqrtNum){
		return Math.sqrt(sqrtNum);
	}
	
	public static int randOf (){
		Random rand = new Random();
		return rand.nextInt(1000); 
	}
	
	public static int divideOf (int divNum1, int divNum2){
		if(divNum1 > 0 && divNum2 > 0){
			return divNum1 / divNum2;
		}
		else{
			return -1;
		}
	}
	
	public static void main(String[] args){ 
		System.out.println(SumOf(9, 9));
		System.out.println(SqrtOf(16));
		System.out.println(randOf());
		System.out.println(divideOf(4, 0));
	}	
}